install:
	virtualenv .env --no-site-packages --distribute --prompt=\(slackbot\)
	. `pwd`/.env/bin/activate; pip install -r requirements.txt

run: 
	. `pwd`/.env/bin/activate; python bot.py
