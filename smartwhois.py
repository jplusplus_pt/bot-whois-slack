#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json


def whois(domain):
    url = 'http://52.90.13.14:5000/v1/domain/%s' % domain
    response = requests.get(url)
    data = json.loads(response.text)

    info = {}

    if data["error"] != 0:
        info["error"] = data["domain"]["message"]
        return info

    values = data["domain"]

    if values.get("data"):
        if values["data"].get("person"):
            info["person"] = values["data"].get("person")
        else:
            info["person"] = []
        if values["data"].get("organization"):
            info["organization"] = values["data"].get("organization")
        else:
            info["organization"] = []
        if values["data"].get("contact"):
            info["contact"] = values["data"].get("contact")
        else:
            info["contact"] = []
        if values["data"].get("contact"):
            info["date_registered"] = values["data"].get("dateRegistered")
        else:
            info["date_registered"] = []

    info["message"] = values["message"].strip()
    info["raw_data"] = values["raw_data"]
    return info


