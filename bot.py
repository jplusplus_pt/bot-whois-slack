from slackbot.bot import Bot, respond_to, default_reply
import requests
from domaininfo.whois import whois
from cleverbot import Cleverbot

cb = Cleverbot()


def parse_slack_url(url, message):
    try:
        url.encode("utf-8")
    except:
        message.reply("I can't deal with your fancy unicode domains, sorry!")
        return None
    return url.strip("<>").split("|")[-1]


def format_block(txt):
    txt = txt.replace("\n\n", "\n  ").strip() + "\n\n"
    return txt


@respond_to('whoisraw (.*)')
def cmd_whois_raw(message, domain):
    domain = parse_slack_url(domain, message)
    if not domain:
        return
    try:
        info = whois(domain)
    except requests.ConnectionError:
        message.reply("I can't connect to the API! Sorry!")
        return
    if info.get("error"):
        output = "Error: %s" % (info["error"])
        message.reply(output)
        return

    output = "\nRaw WHOIS information for *%s*\n\n```%s```" % (domain, info["raw_data"].strip())
    message.reply(output)


@respond_to('whois (.*)')
def cmd_whois(message, domain):
    domain = parse_slack_url(domain, message)
    if not domain:
        return
    try:
        info = whois(domain)
    except requests.ConnectionError:
        message.reply("I can't connect to the API! Sorry!")
        return

    if info.get("error") and not info.get("message"):
        # error only
        message.reply(info.get("error"))
        return

    output = "\n"

    if info.get("message"):
        output += format_block(info["message"])

    for person in info.get("person"):
        output += "> Person: *%s*\n" % (person)
    for org in info.get("organization"):
        output += "> Organization: *%s*\n" % (org)
    for contact in info.get("contact"):
        output += "> Contact: *%s*\n" % (contact)
    for d in info.get("date_registered"):
        output += "> Reg. date: *%s*\n" % (d)
    output += "\n\nIf you want to see all the raw data from the WHOIS request, try `@jbotbot whoisraw %s`." % domain

    message.reply(output)


def get_msg_metadata(message):
    """https://github.com/lins05/slackbot/issues/83#issuecomment-220088918"""
    details = {}
    details["text"] = message.body["text"]
    details["ts"] = message.body["ts"]
    details["user"] = message.body["user"]
    details["username"] = message._client.users.get(message.body["user"])["name"]
    details["team"] = message.body["team"]
    details["type"] = message.body["type"]
    details["channel"] = message.body["channel"]
    return message


@default_reply
def default_handler(message):
    answer = cb.ask(message)
    message.reply("...." + answer)


def main():
    bot = Bot()
    print "Running bot..."
    bot.run()

if __name__ == "__main__":
    main()
