# Whois Slackbot

Run `make install` to get a virtualenv from which to run the bot.

Before running, you'll need a bot key from Slack -- see here how to get one.

After that, either set the `SLACKBOT_API_TOKEN` environment variable, or create a `slackbot_settings.py` with your key inside:

    API_TOKEN = "abcdefghijklmnopqrstuvwxyz1234567890"

## Running

    make run
